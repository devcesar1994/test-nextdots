<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Priority;

class PrioritiesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function index()
    {
        $priorities = Priority::all();
        return response()->json($priorities);
    }

    public function store(Request $request){
        

        $priority = Priority::create($request->all());
        
        if($priority){
            return response()->json($priority);
        }
        
        return response()->json(["No se ha podido guardar la prioridad"]);
    }

    public function show($id)
    {
        
        $priority = Priority::find($id);

        if($priority)
        {
            return response()->json($priority);
        }

        return response()->json(["Prioridad no encontrado"]);

    }

    public function update(Request $request, $id)
    {
        
        $priority           = Priority::find($id);
        $priority->name     = $request->input('name');
        
        if($priority->save())
        {
            return response()->json($priority);
        }
        
        return response()->json(['No se ha podido actualizar la prioridad']);
    }

    public function delete($id)
    {
        
        $priority  = Priority::find($id);
        
        if($priority->delete())
        {
             return response()->json(['Prioridad eliminado correctamente']);
        }
        
        return response()->json(['No se ha podido eliminar la prioridad']);
          
       
    }

}
