<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$app->get('/', function () use ($app) {
    return $app->version();
});



// Routes Users

$app->get('/users','UsersController@index');

$app->post('/user','UsersController@store');
 
$app->get('/user/{id}','UsersController@show');

$app->put('/user/{id}','UsersController@update');
	 
$app->delete('user/{id}','UsersController@delete');


// Routes Priority

$app->get('/priorities','PrioritiesController@index');

$app->post('/priority','PrioritiesController@store');

$app->get('/priority/{id}','PrioritiesController@show');
 
$app->put('/priority/{id}','PrioritiesController@update');
	 
$app->delete('priority/{id}','PrioritiesController@delete');


// Routes Tasks

$app->get('/tasks','TasksController@index');

$app->post('/task','TasksController@store');

$app->get('/task/{id}','TasksController@show');
 
$app->put('/task/{id}','TasksController@update');
	 
$app->delete('task/{id}','TasksController@delete');

